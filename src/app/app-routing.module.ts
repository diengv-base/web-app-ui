import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {HomeComponent} from "./pages/home/home.component";
import {GioiThieuComponent} from "./pages/gioi-thieu/gioi-thieu.component";


const routes: Routes = [
  {
    path: 'gioi-thieu',
    component: GioiThieuComponent
  },
  {
    path: 'bulma',
    loadChildren: () => import('./modules/bulma/bulma.module').then(m => m.BulmaModule)
  },
  {
    path: '**',
    component: HomeComponent,
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}

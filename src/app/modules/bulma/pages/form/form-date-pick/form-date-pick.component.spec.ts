import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormDatePickComponent } from './form-date-pick.component';

describe('FormDatePickComponent', () => {
  let component: FormDatePickComponent;
  let fixture: ComponentFixture<FormDatePickComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormDatePickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormDatePickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

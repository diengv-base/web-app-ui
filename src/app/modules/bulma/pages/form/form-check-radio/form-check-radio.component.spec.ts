import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormCheckRadioComponent } from './form-check-radio.component';

describe('FormCheckRadioComponent', () => {
  let component: FormCheckRadioComponent;
  let fixture: ComponentFixture<FormCheckRadioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormCheckRadioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormCheckRadioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormInputAddonComponent } from './form-input-addon.component';

describe('FormInputAddonComponent', () => {
  let component: FormInputAddonComponent;
  let fixture: ComponentFixture<FormInputAddonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormInputAddonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormInputAddonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormInputIconComponent } from './form-input-icon.component';

describe('FormInputIconComponent', () => {
  let component: FormInputIconComponent;
  let fixture: ComponentFixture<FormInputIconComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormInputIconComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormInputIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KichThuocComponent } from './kich-thuoc.component';

describe('KichThuocComponent', () => {
  let component: KichThuocComponent;
  let fixture: ComponentFixture<KichThuocComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KichThuocComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KichThuocComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

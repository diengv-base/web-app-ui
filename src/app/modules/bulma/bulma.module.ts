import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BulmaRoutingModule} from "./bulma-routing.module";
import { HomeComponent } from './pages/home/home.component';
import { FormInputComponent } from './pages/form/input/form-input/form-input.component';
import { BulmaLayoutComponent } from './bulma-layout/bulma-layout.component';
import { MauSacComponent } from './pages/mau-sac/mau-sac.component';
import { VanBanComponent } from './pages/van-ban/van-ban.component';
import { KichThuocComponent } from './pages/kich-thuoc/kich-thuoc.component';
import { FormSelectComponent } from './pages/form/form-select/form-select.component';
import { FormCheckRadioComponent } from './pages/form/form-check-radio/form-check-radio.component';
import { FormDatePickComponent } from './pages/form/form-date-pick/form-date-pick.component';
import { FormInputDefaultComponent } from './pages/form/input/form-input-default/form-input-default.component';
import { FormInputLabelComponent } from './pages/form/input/form-input-label/form-input-label.component';
import { FormInputIconComponent } from './pages/form/input/form-input-icon/form-input-icon.component';
import { FormInputAddonComponent } from './pages/form/input/form-input-addon/form-input-addon.component';


@NgModule({
  declarations: [HomeComponent, FormInputComponent, BulmaLayoutComponent, MauSacComponent, VanBanComponent, KichThuocComponent, FormSelectComponent, FormCheckRadioComponent, FormDatePickComponent, FormInputDefaultComponent, FormInputLabelComponent, FormInputIconComponent, FormInputAddonComponent],
  imports: [
    CommonModule,
    BulmaRoutingModule
  ]
})
export class BulmaModule {
}

import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {BulmaLayoutComponent} from "./bulma-layout/bulma-layout.component";
import {HomeComponent} from "./pages/home/home.component";
import {FormInputComponent} from "./pages/form/input/form-input/form-input.component";
import {MauSacComponent} from "./pages/mau-sac/mau-sac.component";
import {VanBanComponent} from "./pages/van-ban/van-ban.component";
import {KichThuocComponent} from "./pages/kich-thuoc/kich-thuoc.component";
import {FormSelectComponent} from "./pages/form/form-select/form-select.component";
import {FormCheckRadioComponent} from "./pages/form/form-check-radio/form-check-radio.component";
import {FormDatePickComponent} from "./pages/form/form-date-pick/form-date-pick.component";
import {FormInputDefaultComponent} from "./pages/form/input/form-input-default/form-input-default.component";
import {FormInputLabelComponent} from "./pages/form/input/form-input-label/form-input-label.component";
import {FormInputIconComponent} from "./pages/form/input/form-input-icon/form-input-icon.component";
import {FormInputAddonComponent} from "./pages/form/input/form-input-addon/form-input-addon.component";


const routes: Routes = [
  {
    path: '',
    component: BulmaLayoutComponent,
    children: [
      {
        path: '',
        component: HomeComponent
      },
      {
        path: 'mau-sac',
        component: MauSacComponent
      },
      {
        path: 'van-ban',
        component: VanBanComponent
      },
      {
        path: 'kich-thuoc',
        component: KichThuocComponent
      },
      {
        path: 'form-input',
        component: FormInputComponent,
        children: [
          {
            path: '',
            redirectTo: 'lv1',
            pathMatch:'full'
          },
          {
            path: 'lv1',
            component: FormInputDefaultComponent,
          },
          {
            path: 'lv2',
            component: FormInputLabelComponent,
          },
          {
            path: 'lv3',
            component: FormInputIconComponent,
          },
          {
            path: 'lv4',
            component: FormInputAddonComponent,
          }
        ]
      },
      {
        path: 'form-select',
        component: FormSelectComponent
      },
      {
        path: 'form-check-radio',
        component: FormCheckRadioComponent
      },
      {
        path: 'form-date-pick',
        component: FormDatePickComponent
      }
    ],
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BulmaRoutingModule {
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BulmaLayoutComponent } from './bulma-layout.component';

describe('BulmaLayoutComponent', () => {
  let component: BulmaLayoutComponent;
  let fixture: ComponentFixture<BulmaLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BulmaLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BulmaLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {Component, OnInit, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'app-bulma-layout',
  templateUrl: './bulma-layout.component.html',
  styleUrls: ['./bulma-layout.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class BulmaLayoutComponent implements OnInit {
  title = '';
  subTitle = '';

  constructor() {
  }

  ngOnInit(): void {
  }

}
